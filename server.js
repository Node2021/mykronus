require('dotenv').config();
const express = require("express");
const bodyParser = require("body-parser");
const session = require('express-session');
const cookieParser = require('cookie-parser');
var cors = require('cors');
const path = require('path');
const axios = require('axios');
const http = require('http');

const url = require('url');
///const popupS = require('popups');

const app = express();
const router = express.Router();



app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static("public"));
app.set("view engine", "ejs");

app.use(
    session({
        name: 'sid',
        saveUninitialized: false,
        resave: false,
        secret: `it's a secret!`,
        cookie: {
            maxAge: 1000 * 300 ,
            sameSite: true,
            secure: true
        }
    }), 
) 

// Read the host address and the port from the environment
///const hostname = process.env.HOST;
const port = process.env.PORT;

//const server = http.createServer((req, res) => {
    //var q = url.parse(req.url, true);
    //console.log("q-->"+q.pathname);
   // res.statusCode = 200;
   // res.setHeader('Content-Type', 'application/json');
   // res.end(`{"message": "Hello World"}`);
    
//}).listen(8080);

/* var corsOptions = {
    origin: 'https://newmykronus.colabus.com',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
} */

app.use(express.static(path.join(__dirname,"public")));

/* app.use(function(req, res, next) { 
    res.header("Access-Control-Allow-Origin", "*"); 
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"); 
    next(); 
}); */

/* app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "https://newmykronus.colabus.com"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  }); */

  //app.use(cors());
  //app.options('*', cors());  

app.get("/", function(req, res){
    //res.sendFile(path.join(__dirname + '/views/login.html'));
    //res.sendFile(path.join(__dirname + '/views/login.ejs'));
    res.render("login.ejs",{
        lighttpdpath1: process.env.lighttpdpath,
        sessionmaxAge:req.session.cookie.maxAge,
        sessionid:req.session.id
    });
    
    
});

app.get("/login", function(req, res){
  
    res.render("login.ejs",{
        lighttpdpath1: process.env.lighttpdpath,
        sessionmaxAge:req.session.cookie.maxAge,
        sessionid:req.session.id
    });
   
});

app.get("/register", function(req, res){
     res.render("register.ejs");
    //res.sendFile(path.join(__dirname + '/views/registration.html'));
});

app.get('/postlogin',function(req,res){
    
    let sessionval = { 
        sessionmaxAge:req.session.cookie.maxAge,
        sessionexpire:req.session.cookie.expires
    }
    
    console.log(req.session.cookie.maxAge+"<--->"+req.session.cookie.expires);
    res.cookie("sessionData", sessionval); 
    res.render("postlogin.ejs",{
        lighttpdpath1: process.env.lighttpdpath,
        sessionmaxAge:sessionval.sessionmaxAge,
        sessionexp:sessionval.sessionexpire,
        sessionid:req.session.id
    });

});


//var globals = require('globals.js'); // << globals.js path
//console.log("inside1111"+globals.domain); // << Domain.
//app.get('/globals',function(req,res){
 //   console.log("inside globals");
  // res.render("globals.js");
//});
 //router.get('/postlogin', function(req, res) {
  //  res.render('postlogin.ejs');
 // });

 /* cors.createServer({
    originWhitelist: [], // Allow all origins
    requireHeader: ['origin', 'x-requested-with'],
    removeHeaders: ['cookie', 'cookie2']
}).listen(port, host, function() {
    console.log('Running CORS Anywhere on ' + host + ':' + port);
}); */

app.listen(port, () => {
    
    console.log(`Server running at http://${port}/`);
}); 

