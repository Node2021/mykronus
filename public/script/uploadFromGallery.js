/**
 * 
 */


     var AlbUpId;
	 var menuType;
	 function getUserAlbums(Id,type){
	   $('#loadingBar').show();
	   timerControl("start"); 
	  $("#userProfileImagesContentDiv").html("");
	  AlbUpId=Id;
	  menuType=type;

	  $("#transparentDiv").show();
      $("#userProfileImagesContentDiv").css("z-index","1000");
      $("#userProfileImagePopup").show();
      $("#sysAdminUploadOptns").hide();
      $.ajax({
		  url: path +'/postLoginAction.do',
		  type:"POST",
		  data:{act:"getUserAlbums"},
		  error: function(jqXHR, textStatus, errorThrown) {
               		checkError(jqXHR,textStatus,errorThrown);
               		$("#loadingBar").hide();
					timerControl("");
					},
		  success:function(result){
			 checkSessionTimeOut(result);
			 if(result == ''){
				var data='<div align="center" style=" margin-top: 130px; font-size: 17px;">No photos found</div>'
				$("#userProfileImagesContentDiv").html(data);
		     }else{
			    $("#userProfileImagesContentDiv").html(result);
	    	    /*if(!isiPad){
			       popUpScrollBar('userProfileImagesContentDiv');
			    }*/
		     }
		     $('#loadingBar').hide();
	  		 timerControl("");
		  }
	  });
    }
	 
	function getAlbumPhotos(albumId,userId,albumName){
	       $("#userProfileImagesContentDiv").html("");
	 	   $('#loadingBar').show();
	 	   timerControl("start");
	 	   $("#userPrfHeader").append('>'+albumName);
	 	   $("#uProfileImgBack").show();
	 	   $.ajax({
	 			url:path+"/postLoginAction.do",
	 			type:"POST",
	 			data:{act:"getAlbumPhotos",userId:userId,albumId:albumId},
	 			error: function(jqXHR, textStatus, errorThrown) {
	               		checkError(jqXHR,textStatus,errorThrown);
	               		$("#loadingBar").hide();
						timerControl("");
						},
	 			success:function(result){
	 			   checkSessionTimeOut(result);
	 			    if(result == ''){
	 			        var data='<div align="center" style="margin-top: 130px; font-size: 17px;">No photos found</div>'
	 			        $("#userProfileImagesContentDiv").html(data);
	 			    }else{
	 				    $("#userProfileImagesContentDiv").html(result);
	 				    if(!isiPad){
	 				       popUpScrollBar('userProfileImagesContentDiv');
	 				    }
	 				}
	 				$('#loadingBar').hide();
	 	    		timerControl("");
	 			}
	 		});
	 	}
	
	 function closeUserProfileImgPopup(){
	      $('#loadingBar').show();
		  timerControl("start");
	      $("#userProfileMain").css("z-index","");
		  $("#Profile_Upload").hide();
	      $("#userProfileImagePopup").hide();
	      $('#loadingBar').hide();
	      $("#transparentDiv").hide();
		  timerControl("");
	    }
	
        var defaultphotoId="";
	    var defaultlovType="";
	    var orgImgExtesion = ""; 
	    var uPhotoId="";
	    var uDefaultType="";
		function setUserProfilePic(photoId,photoType,userId,lovType,imgPath){
		
		       $.ajax({
					url:path+"/adminUserAction.do",
					type:"POST",
					data:{act:"sysAdminuploadFromGallery",userId:userId,photoType:photoType,photoId:photoId,lovType:lovType},
					error: function(jqXHR, textStatus, errorThrown) {
	                		checkError(jqXHR,textStatus,errorThrown);
	                		$("#loadingBar").hide();
							timerControl("");
							},
					success:function(result){
						//alert("Result---"+result);
						checkSessionTimeOut(result);
						//fileExtension=result;
						
						orgImgExtesion=photoType;
						defaultphotoId=photoId;
				        defaultlovType=lovType;
				        
				        fileExtension=photoType;
					    uPhotoId=photoId;
					    uDefaultType=lovType;
					    
					    ImageUploadProjectFlag = photoType;
					    
						var d=new Date();
						if(result != ''){
							
							/** add ur div to show the gallery pic in ur module **/
							$("img#profileImgProfile, img#hideDiv, img#hideDiv1").attr("src",imgPath+"?"+d.getTime());
							$("img#compImage").attr("src",imgPath+"?"+d.getTime()); 
							$("input#imgUrlForComp").val(imgPath+"?"+d.getTime());
							$("img[id^=hideDiv_]").attr("src",imgPath+"?"+d.getTime());  // in project description gallery upload
							$("img[id^=hideDiv1_]").attr("src",imgPath+"?"+d.getTime());
			
							projectImgUploadFromGallery = true; // for upload from project description
							ImageIsUploaded = true; // for upload from group chat
							
							  $("#userProfileMain").css("z-index","");
							  // $('#ideaTransparent').css('display','none');
							   $("#nInputHiddenValCover").val('');
							   $("#userProfileImagePopup").hide();
							   $('#Profile_Upload, #Project_Upload, #Pro_Img_Upload, #chat_Upload').hide(); // hiding the option popUp
							   
							   $("input#hiddenImageCompDetails").val("1");
							   
				              /** put your condition for transparent div to close 
				               *   if you are calling from any popup that time U dnt have to hide the div
				               *   other wise U have to hide the div
				               *  **/
							   if($('#userDetailpopUp').css('display') != "block" && $('#newProjContent').css('display') != "block" ){
								   $("#transparentDiv").hide();
							   }
							   
						}
						
					    parent.$('#loadingBar').hide();
		  				parent.timerControl("");
					}
			  });
		    }
	