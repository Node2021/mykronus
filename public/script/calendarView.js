   	var Path;
   	var Labels;
   	var UserId;
   	var myTask;
	var assgndTsk;
	var historyTsk;
	var lighttpdpath;
	var userType;
	var taskMsg;
	var title = ""; 
	var start = "";
	var end = "";
	var stDate="";
	var startTime = "";
	var endTime = "";
	var repeat = "";
	var description = "";
	var update='';
	var eId='';	
	var calendar = null;
	var curr_event = null;
	var count = 0;
	var priorityId="";
	var modelId="";
	var mid = "";
	var projId = 0;
	var reminder = "";
	var reminderType= "";
	var selectedTaskemailId="";
	var selectedOtherEmail="";
	var removeEmailId="";
	var parId="";
	var myTaskId="";
	var docTaskType="";
	var headerText="";
	var width="";
	var height="";
	var isHistory="";
	var recPatternValue = '';
	var recStart = '';
	var recEnd = '';
	var recType = '';
	var endId="noEnd";
	var sortTaskType='';
	var limit=0;
	var stepVal='';
	var tHour = '0';
	var tMinute = '0';
	var estMin='';
	var serverHour = '';
	var isiPad = '';
	var searchWord = '';
	
	var assignedScroll=true;
	var historyScroll=true;
	var myTaskScroll = true;
	
	var latestCmntDate='';		    /* update Assigned task variables*/
	var taskType1 = '';
	var reminder = '';
	var remType = '';

	var tAction = '';			    /* updateMyTasks variables*/	
	var myComment = '';
	var taskCompAmount = '';
	var workingHours = '';
	var taskAction = '';
	var cmntDate='';
	var cmntId='';

	var commentXml='<Comments>';	/* variable for cancelUpdate*/
	var valueStatus="";
	
	var cntntVal = '';				/* addParticipantEmail method variable*/
		
	var tmpSDate = "";				/* variable for updateEpicTasks method*/
	var tmpEDate = "";
	var taskHour = "";
	var aHour = "";
	
	var saveTaskFlag;               /*variable for testEstimatedHour method */
	
	var tskId = '';					/*   variable for deleteEvent  */
	var tskType ='';
	
	var userEmailId='';				/* removeUpdateParticipant */
	var delUserId='';
	var delTaskId='';
	var objVal='';					
	
    var deletedUserID=",";         /* removeTaskUpdateParticipant vaiable*/
 	var sprintUId = '';
	
    var removeotherEmailId='';	   /* removeOtherPrtcipant variable  */
    var otherObj='';
	
	var startDate = '';			  /*variable for showTaskHoursPopup*/
    var endDate = '';
    							
    var resetDate = 0;			  /* variable for assignedDateReset method  */	
    
    var prevSDate='';			  /* variable for loadStoryDate method */	
 	var prevEDate='';  			  
    								
	 var taskDateXml="";		  /* variable for getDateXml method */
	 
	 
	 var totalHr='';
	 var totalMin='';
	 var estmHour=0;
	 var estmMinute=0;
	 
	 var wFTaskDateXml="<WFTaskDates>";		/* variable for getWFDateXml method */
     var j = 0;
     
     var documentId = '';					/* variables for downloadFile */
	 
	 var messageUserId = '';				/*variables for openMessagePopup */
	function setPath(path,uId){
		Path = path;
		UserId=uId;
	}
	function setLabels(labels){
		Labels=labels
	} 
	function setLightTpdPath(lightTpdpath){
		lighttpdpath=lightTpdpath;
	}
	function setUserRegType(userRegType){
		userType=userRegType;
	}
	function setMsg(tskMsg){
		taskMsg=tskMsg;				
	}
	function addTaskToCalendar(event){							
	    calendar = $('#calendar');
		var events = event;
        calendar.css('font-size','10px !important');
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();							
		var y = date.getFullYear();
		$("#loadBar").remove();
		calendar.fullCalendar({				/*Add Tasks from the database to the Calendar */
        	events: eval(events),
		    width: width-250,
		    height: height-105,
		    disableResizing: true,
		    disableDragging: true,
		    eventRender: function(event, element, calEvent) {
				if(!event.end) 
					event.end = event.start;				
				if (event.editable) {
           			element.css('cursor', 'pointer');
        		}
        		
        		$.fullCalendar.formatDate(event.start, 'MM-dd-yyyy'); /* Task start Date Format */
				$.fullCalendar.formatDate(event.end, 'MM-dd-yyyy');
				element.find(".fc-event-time").before($("<span id=\""+event.id+"\" depId=\""+event.id+"\" class=\"fc-event-icons\" style=\"padding-right:2px; float:left;\"></span>").html("<img src=\""+Path+"/images/calender/"+event.taskImage+".png\" style=\"height:15px;width:17px;\" title=\""+event.taskImage+"\" >"));
				element.find(".fc-event-title").after($("<div id=\"createDep_"+event.id+"\" onclick=\"connectorDep(this);event.stopPropagation();\" style=\"width:10px;height:5px; float:right;\"></div>"));
				if(event.depTaskId!="" && event.depTaskId!="-"){
				var date =  $.fullCalendar.formatDate(event.end, 'dd-MM-yyyy');
					element.find(".fc-event-title").after($("<span id=\"depIds_"+event.depTaskId+"\" depId=\""+event.id+"\"  parentEndDate=\""+date+"\" onclick=\"calendarDep(this);event.stopPropagation();\" class=\"depTaskShowIcon\" style=\"padding-right:2px; float:right;\"></span>").html("<img src=\""+Path+"/images/calender/depdencyWhite.png\"  style=\"height:14px;margin-top:2px;margin-right: 10px;\" title=\""+event.taskImage+"\" >"));
				//	element.find(".fc-event-title").after($("<span depId=\""+event.id+"\" parentEndDate=\""+date+"\" onclick=\"event.stopPropagation();\" class=\"depTaskShowIcon\" style=\"padding-right:2px; float:right;\"></span>").html("<img src=\""+Path+"/images/addNotes.png\"  style=\"height:14px;width:15px;margin-top:2px;margin-right: 10px;\" title=\""+event.taskImage+"\" >"));
				}
				if($('#dd span').text() == 'Assigned Tasks') {	/* Add delete icon to Assigned Tasks */
					element.find(".fc-event-title").after($("<span class=\"fc-event-icons\" style=\"padding-right:2px; float:right;\"></span>").html("<img src=\""+Path+"/images/calender/delete.png\" onclick=\"deleteEvent("+event.id+",'"+event.taskType+"');\" style=\"height:15px;width:17px;\" title=\"Delete\" >"));
	        	//	element.find(".fc-event-title").css("color","black");
	        	//	element.find(".fc-event-time").css("color","black");
	        	}
	        	
	        	$(".fc-event-time").hide();
	        	$('.fc-event').attr('id',"setDep_"+event.id);	        	
        	},
        	header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			windowResize: function(view) {
        	},
        	eventMouseover: function(event, jsEvent, view) {
		        if (view.name !== 'agendaDay') {
		            $(jsEvent.target).attr('title', event.title);
		        }
    		},
			weekMode: 'liquid',
			timeFormat: 'hh:mm tt',
			editable: true,
			ignoreTimezone: true,
			dayClick: function(date, allDay, jsEvent, view) { /* Opens the popup on Dayclick */
			$("#uploadDocTaskId").val('');
			tflag=true;
			$("#taskDocCreateImg").show();
			
				$('.Create').text(getValues(Labels,'Save'));
				$('.Create').attr('title',getValues(Labels,'Save'));
				var currentDate = new Date();
				var day = currentDate.getDate();
				var month = currentDate.getMonth() + 1;				
				var year = currentDate.getFullYear();
				var hour = currentDate.getHours();
				var minute = currentDate.getMinutes();
				var stTime = hour+':'+minute;
				update = 'no';
				defaultCss(); //---------------->setting default css for the task creation popup
				getAllUsers();
				$('.calInstructions').text($("#taskMsg").val());			
				$('#startDateDiv').val('');
				if(userType == 'web_standard')
					$('#selectTask > .Work_Flow').hide();
				else
					$('#selectTask > .Work_Flow').show();
	   			
	   			$("#eventPopup").show();
	   			$('#eventContainer').show();
	   			$('#actualHourContentDiv').show();
	   			$('#projectUsers').show();
	   			//docTaskclosePopup();
	   			$('.estmHour').show();
	   			timePicker();
	   			$('#pariticipateDiv').show();
				stDate = $.fullCalendar.formatDate(date, 'MM-dd-yyyy');
				var sendDate = $.fullCalendar.formatDate(date, 'yyyy/MM/dd');
				sendDate = new Date(Date.parse(sendDate.replace(/-/g,' ')))
				today = new Date();
				today.setHours(0,0,0,0)
				if (sendDate < today) {
					stDate = $.fullCalendar.formatDate(today, 'MM-dd-yyyy');
				} 
			//	$('.To').css("margin-left","23px");
//  Incomplete Task (24/09/2105)*********************************
			//	$('#startDateDiv').val(stDate);					
			//	$('#endDateDiv').val(stDate);
				$('#createEvent').removeClass().addClass('createEvent');
	   			$('#wFTaskDateSave').css('display','none');
	   			$('#taskDateSave').css('display','block');
	   			$('.createEvent').attr('onclick','addTasks();');
				$('.transparent').show();
				parent.$("#transparentDiv").show();
				$('select#project').val(getValues(Labels,"All_Users"));
				dayObj = $(this);
				var el = document.getElementById('createButton');
			    el.scrollIntoView(true);
			    initCal();
			    getAllProject();
			    $("#dependencyTask").show();
			    $('#dependencyTask').attr("onclick","dependencyTaskPopUp()");
			    $('#hiddenDependentTaskIds').val('');
				$('#selectedTaskDiv').html('');
				$("#docIconNameLevel").hide();
				$("#eventName").css("width",'412px');
			    if(height < 550){
				    var isiPad = navigator.userAgent.match(/iPad/i) != null;
		    		if(!isiPad)	{
						scrollBar('eventPopup');
					}	
				}
			},
			eventClick: function(calEvent, jsEvent, view) { /* Opens The particular Task Popup */
	   			curr_event = calEvent;
	    		var task_id = curr_event.id;
	    		eId = task_id;					
	    		var divId = "";
	    		divId = curr_event.taskType;
	    		
	    		if(divId == 'assignedSprint') {
	    			var ideaId = curr_event.epicId;
	    			updateEpicTasks(task_id,ideaId);
	    		}
	    		else
	    			viewTask(task_id,calEvent,divId);
	    			update="yes";
	    			
	    			
			},
				eventMouseover: function(calEvent, jsEvent, view) {
                $(this).css("cursor","pointer");
           },
        });	
       /* 	 $(".depTaskShowIcon").each(function(){		
	        	var parentId=$(this).attr('depId');
	        	var groupDepId=$(this).attr('id').split("depIds_")[1];		
	        	var eachId=groupDepId.split(",")
	        	for(var i=0;i<eachId.length;i++){							
	        		jqSimpleConnect.connect("#createDep_"+parentId, "#createDep_"+eachId[i], {radius: 5, color: 'black'});
	        		alert("parentId::"+parentId+"inside::"+eachId[i]);
	        	}
	        		
	        });	*/
	   
        	$('span.fc-button-prev,span.fc-button-next,span.fc-button-month,span.fc-button-basicWeek,span.fc-button-basicDay,span.fc-button-today').click(function() {
			scrollBar('calendarContainer');			
		});
	/*	$('.depTaskShowIcon').bind('click', function (events) {
			//		$(".fc-event-inner").css("background","#2EB5E0").css("border","#2EB5E0").parent('div').css("background","#2EB5E0").css("border","#2EB5E0");		
    				var id=$(this).attr('depId');
    				var parentEndDate=$(this).attr('parentEndDate');
    				$(this).parents(".fc-event-inner").css("background","#067886").css("border","#067886").parent('div').css("background","#067886").css("border","#067886");;
    				showDepTask(id,parentEndDate);				
					events.stopPropagation();
   		});
   		$('.depTaskShowIcon').bind('click', function (events) {						
   		//	  jqSimpleConnect.connect(this, ".fc-tue", {radius: 8, color: 'green'});
    		  $(this).live('hover',function() {
   			  hoveredId = $(this).attr('class');
   			 // $('#hoveredId').html(hoveredId);
			  });
   		});*/
   	
		$('.fc-header-left + .fc-header-center').before($("<td class=\"fc-header-dropdown\"><section class=\"main\" style=\"width:10em;padding-left: 0.5em;\"><div class=\"wrapper-demo\"  style=\"width:10em\"><div id=\"dd\" style=\"padding:0.3em 0.7em;height:17px;\" class=\"wrapper-dropdown-2\" tabindex=\"1\"><span style=\"float:left;width:100px;height:25px;overflow:hidden;\">"+getValues(Labels,'MY_TASKS')+"</span><ul class=\"dropdown\"><li><a href=\"#\" id=\"Assigned Tasks\" title=\""+getValues(Labels,'ASSIGNED_TASKS')+"\">"+getValues(Labels,'ASSIGNED_TASKS')+"</a></li><li><li><a href=\"#\" id=\"My Tasks\" title=\""+getValues(Labels,'MY_TASKS')+"\" >"+getValues(Labels,'MY_TASKS')+"</a></li><li></ul></div></div></section></td>")); /* Task Dropdown */
		var dd = new DropDown( $('#dd') );
		var dd = new DropDown( $('#dd5') );
		var dd = new DropDown( $('#dd8') );
		var dd = new DropDown( $('#dd9') );
		var dd = new DropDown( $('#dd10') );
		var dd = new DropDown( $('#docType') );
		var dd = new DropDown( $('#recdd') );
		var dd = new DropDown( $('#recdd1') );
		var dd = new DropDown( $('#recdd2') );
		var dd = new DropDown( $('#recdd3') );
		var dd = new DropDown( $('#recdd4') );
		var dd = new DropDown( $('#recdd5') );
		var dd = new DropDown( $('#recdd6') );
		$(document).click(function() {
			$('.wrapper-dropdown-2').removeClass('active');
		});
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad)	
			scrollBar('calendarContainer');
		$("#uploadDocTaskId").val('');	
		

}
function calendarDep(obj){
					if($('#dd span').text()== "My Tasks"){
						$(".fc-event-inner").css("background","#2EB5E0").css("border","#49BFE6").parent('div').css("background","#2EB5E0").css("border","#49BFE6");		
    				}else{
						$(".fc-event-inner").css("background","#F5CB02").css("border","#FBAD3D").parent('div').css("background","#F5CB02").css("border","#FBAD3D");		
					}
			//		$(".fc-event-inner").css("background","#2EB5E0").css("border","#2EB5E0").parent('div').css("background","#2EB5E0").css("border","#2EB5E0");		
    				var id=$(obj).attr('depId');
    				var parentEndDate=$(obj).attr('parentEndDate');
    				$(obj).parents(".fc-event-inner").css("background","#067886").css("border","#088090").parent('div').css("background","#067886").css("border","#088090");;
    				showDepTask(id,parentEndDate);				
				//	events.stopPropagation();
}
var connectorX="";
var connectorY="";
function connectorDep(obj){			
	if(connectorX==""){
		connectorX = $(obj).attr('id');
	}
	else{
		connectorY=$(obj).attr('id');
	}
	if(connectorX!="" && connectorY!=""){
		jqSimpleConnect.connect("#"+connectorX, "#"+connectorY, {radius: 5, color: 'black'});
		connectorX="";
		connectorY="";
	}
	
}
function taskEDclick(obj){    /* Show or Remove Unchecked Workflow Steps  */
	var objId = $(obj).attr('id');
	if(objId == 'dht'){
		$('#taskWFDetails div.wFinactive').show();
	}if(objId == 'hht'){
		$('#taskWFDetails div.wFinactive').hide();
	}
	scrollBar('taskWFDetails');
}


function removeOtherPrtcpnt(obj,email) {        /* Delete Other than company users from Event */
	$(obj).parent().remove();
	var deleteId = email;
	selectedOtherEmail = selectedOtherEmail.substring(0, selectedOtherEmail.indexOf(',' +deleteId)) + selectedOtherEmail.substring(selectedOtherEmail.indexOf(',' +deleteId) + deleteId.length + 1, selectedOtherEmail.length);
}


/*
function changeType(obj) { //Change type of tasks(task, workflow, event) 	
	$('.addParticipant').hide();
	$('.createEvent').css('display','block');
	$('.createEvent1').css('display','none');	
	var image = $('#selectTask').val();
	headerText = image.toLowerCase();
	emptyTaskDetails();
	if(headerText == 'workflow') {
		mid='';
		parId = 'workflow';
		$('.calInstructions').text($("#taskWfMsg").val());
		$('#userSearch').val('');
		$('#workFlowName').val('');
		$('#searchUserImg').attr('src',''+Path+'/images/calender/search.png');
		$('#taskDetails').css('display','none');
		$('#taskWFDetails').empty();
		$('#wfShowTask').empty();
		$('#eventPopup').removeClass('eventPopup').addClass('workFlowPopup');
		$('#createOrCancel').css('width','150px');
		$('.wFContainer').css('height','');
		$('#createEvent #cancelEvent').css('width','75px');
		$('.addParticipant').css('diaplay','none');
		$('#addParticipantTasks').css('display','none');
		$('#taskDateSave').css('display','none');
		$('#wFTaskDateSave').css('display','block');
		$('input#workFlowName').css('');
		$('input#workFlowName').css('border','');
		$('.transparent').show();
		parent.$("#transparentDiv").show();
		$('#selTemplate').html(''); 
		$.ajax({
			url: Path+'/calenderAction.do',
       		type:"POST",
	 		data:{act:'createWorkFlowTask'},
			success:function(result){
			    parent.checkSessionTimeOut(result);
			    var wFTaskHtml = result;
			    $('#eventPopup').css('display','block');
			    $('#workFlowContainer').css('display','block');
				$('#selTemplate').append(wFTaskHtml);
				var dd = new DropDown( $('#dd7') );
				$(document).click(function() {
		       		$('.wrapper-dropdown-2').removeClass('active');
		       	});
	  			parent.$('#loadingBar').hide();
		        parent.timerControl("");}
		   });
	}
	if(headerText != 'workflow') {
	    $('#workFlowContainer').css('display','none');
	    $('#wFTaskDateSave').css('display','none');
	    $('#eventContainer').css('overflow','hidden');
		$('#userSearch').val('');
		$('#searchUserImg').attr('src',Path+'/images/calender/search.png');
		$("#eventContainer").find('.mCSB_scrollTools').css('display','none'); 
		
		$('#taskDateSave').css('display','block');
		$('#taskDetails').css('display','block');
		$('#addParticipantTasks').css('display','block');
		$('#dd7 span').text(getValues(Labels,'Select'));
	}
	if(headerText != 'event'){ 
	  $('.selectPlace').hide();
	  $('.estmHour').show();
	  $('#emailDiv').hide();
	}
	if(headerText == 'task'){	
		parId = 'task';
		$('#actualHourContentDiv').css('display','block');
     	$('.calInstructions').text($("#taskMsg").val());
		$("#participantEmail").css('height','145px');
		$('#projectDiv').css('padding-top','0px');
		$('#eventPopup').removeClass('workFlowPopup').addClass('eventPopup');
		$('.eventDateDiv').hide();
		$('#endDateDiv').val(stDate);
		$('.taskDate').show();
		$('#pariticipateDiv').css('padding-top','10px');
		$('#addParticipantTasks').removeClass().addClass('addTaskEmail');
		$('#titleText').text(getValues(Labels,'Task'));
	}
	if(headerText == 'event') {
		parId = 'Event';
        $('.calInstructions').text($("#taskEventMsg").val());
        $('#actualHourContentDiv').css('display','none');
		$('#emailDiv').show();
        $('.taskDate').hide();
        $('.estmHour').hide();
        $('#eventPopup').removeClass('workFlowPopup');
		$('#eventPopup').addClass('eventPopup');
		$('#addParticipantTasks').removeClass();
		$('#addParticipantTasks').addClass('addEventEmail');
		$('.eventDateDiv').show();
		$('#pariticipateDiv').css('padding-top','20px');
		$('#addParticipantTasks').css('height','173px');
		$("#participantEmail").css('height','115px');
		$('#taskDetails').css('height','277px');
		$('#startEventDate').val(stDate);
		$('#endEventDate').val(stDate);
		initEventCal();
		timePicker();
		$('.selectPlace').show();	
		$('#titleText').text(getValues(Labels,'Event'));
		$('#titleImg').attr('src',Path+'/images/calender/'+image+'Black.png');
	}
	$('#titleImg').attr('src',Path+'/images/calender/'+image+'Black.png');
	}

*/


function customScroll(){
  $(".wFParticipantEmail").mCustomScrollbar({
	  scrollButtons:{
		  enable:true
	  }
  	});
}	

/*
var selectedFlowImage=0;	
var tempID=0;
function initWFTaskImagePopup(){	
			var sId = mid;
			tempID = mid;
			customLabels = Labels;
			if(sId!=null && sId!=''){
				//loadWFLevelContent();
				showTemplateDetails('view','Calendar');
            }else{
				parent.alertFun(getValues(customalertData,"Alert_SelectTemplate"),'warning');
				return false;
			}
}*/
		
/*function scrollTemplate(id) {
	$("div#" + id).mCustomScrollbar({
		scrollButtons : {
			enable : true
		},
	});
	$('div.mCSB_container').css('margin-right', '0px');
	$('div#' + id + '.mCSB_container').css('margin-right', '15px');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_buttonUp').css(
			'background-position', '-80px 0px');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_buttonDown').css(
			'background-position', '-80px -20px');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_buttonUp').css('opacity', '0.6');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_buttonDown')
			.css('opacity', '0.6');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_draggerRail').css('background',
			'#B1B1B1');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css(
			'background', '#6C6C6C');
	$('div#' + id + ' .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar')
			.css('background', '#B1B1B1');
	$(
			'div#'
					+ id
					+ ' .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar')
			.css('background', '#B1B1B1');

}		*/
 /*
 function validateRequiredFields(){
	  var status='valid';
	  $("div.wFactive").each(function (i) {
	   var idd=$(this).attr('id');
	   var ids=idd.split('_')[1];
	   var wFStartDate = $('#wFStart_'+ids).val();
	   var wFEndDate = $('#wFEnd_'+ids).val();
	   var wFStartTime = $('#wFStartTime_'+ids).val();
	   var wFEndTime = $('#wFEndTime_'+ids).val();
	   if($('#wFStart_'+ids).val()== "" || $('#wFStart_'+ids).val()== null || 
	 	$('#wFEnd_'+ids).val() == "" || $('#wFEnd_'+ids).val() == null ||
	 	$('#wFParticipantEmail_'+ids).find('div[id^=wFEmail]').length == 0)
	 	{	
	 	     status="invalid";
	 	}
	 	var sdArray=wFStartDate.split('-');
				var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
				var edArray=wFEndDate.split('-');
				var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
				var d1=new Date(sd);
				var d2=new Date(ed);
				if(d1 > d2){
				   status="invalidDate";
				  $('#wFStart_'+ids).css("border","1px solid red");
				  $('#wFEnd_'+ids).css("border","1px solid red");
				  return false;
				}
		if(wFStartDate == wFEndDate) {
			var st = parseInt(wFStartTime.replace(':', ''), 10); 
        	var et = parseInt(wFEndTime.replace(':', ''), 10);
			if(st >= et){
               $('#wFEndTime_'+ids).css("border","1px solid red");
               status="invalidEndTime";
               if($("div#taskWFDetails").find('div.mCSB_scrollTools').css('display') == 'block'){ 
	        	    $("div#taskWFDetails").mCustomScrollbar("scrollTo","#wFContents_"+ids,{
						 scrollInertia:0,
						 callbacks:false
				    });
			   }
               return false;
            }
            else{		
            	 $('#wFEndTime_'+ids).css("border","1px solid #BFBFBF");}
		}
	});
	 return status;
}*/
function loadWFTaskImageContent(tId){	/* Load template image */
        var lightpdPath = lighttpdpath;
		var imgObj = '<img src="'+lightpdPath+'models_orig//'+tId+'.png" style=/"text-align:center/" />';
	    $('#eventContainer').css('display','none');
		$('#projectUsers').css('display','none');
		$("#wfTaskTemplateView").css('display','block');
		$("#wFTemplateImage").html(imgObj);
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	    if(!isiPad){
	    	customScroll();
		    setTimeout(function() {scrollBar("wFTemplateImage")},500);
		}    
}
function cancelTemplateView(){
       $("#wfTaskTemplateView").css('display','none');
       $('#eventContainer').css('display','block');
	   $('#projectUsers').css('display','block');
}

/*
function constructTaskXml(){		//Construct xml for workflow Tasks 
		var wFTaskEmail="";
		var tData = "<taskData>";
	    jQuery("div.wFactive").each(function (i) {
	    	var ids=$(this).attr('id');
	        var tId=ids.split('_')[1];
	        var WFTaskMail="";
	        $(".wFEmail_"+tId).each(function(i) {
	        	var divId = $(this).attr('id').split('_')[1];
	        	if(WFTaskMail) {
	        		WFTaskMail = WFTaskMail+','+'N:'+divId+',';
	        	}
	        	if(!WFTaskMail){
	        		WFTaskMail = 'N:'+divId;
	        	}	
			});
			tData +="<taskSet id=\""+tId+"\">"
			tData +="<tInstructions>"+$('#wFDescription_'+tId).val()+"</tInstructions>";
			tData +="<tDefinition>"+$('#taskDefinition_'+tId).parent().attr('title')+"</tDefinition>";
			tData +="<tUsersId>"+WFTaskMail+"</tUsersId>";
			tData +="<tStartDate>"+$('#wFStart_'+tId).val()+"</tStartDate>";
			tData +="<tEndDate>"+$('#wFEnd_'+tId).val()+"</tEndDate>";
			tData +="<tStartTime>"+$('#wFStartTime_'+tId).val()+"</tStartTime>";
			tData +="<tEndTime>"+$('#wFEndTime_'+tId).val()+"</tEndTime>";
			tData +="<tReminder>"+$('#remDrpDown_'+tId).val()+"</tReminder>";
			tData +="<tReminderType>"+$('#remTypeDrpDown_'+tId).val()+"</tReminderType>";
			tData +="<tEstmHour>"+$('#estmatedHour_'+tId).val()+"</tEstmHour>";
			tData +="<tEstmMin>"+$('#estmatedMinute_'+tId).val()+"</tEstmMin>";
			tData +="<tLevelId>"+$('#hiddenLevelId_'+tId).val()+"</tLevelId>";
			tData +="</taskSet>";
		});
		tData +="</taskData>"
		return tData;
	} 
*/
var listTaskIdNew="";
function taskCalendar(tName) {
	parent.$('#loadingBar').show();
	parent.timerControl("start");
	calendar.fullCalendar('removeEvents').fullCalendar('removeEventSources'); 
            	var asgndTasks="";
            	if(tName == getValues(Labels,'ASSIGNED_TASKS')) {
            		$.ajax({ 
				       url: Path+'/calenderAction.do',
				       type:"POST",
				       data:{act:'fetchAssignedTaskData'},
				       error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
				       success:function(result){
				       	parent.checkSessionTimeOut(result);
				       		asgndTasks = result;				
				       		calendar.fullCalendar('addEventSource',eval(asgndTasks));
			        		$('#calendar').fullCalendar('render');
			        		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        				if(!isiPad)
			        			$("#calendarContainer").mCustomScrollbar("update"); 
			        			$('.depTaskShowIcon').bind('click', function (event) {				
	        		//		$(".fc-event-inner").css("background","#2EB5E0").css("border","#2EB5E0").parent('div').css("background","#2EB5E0").css("border","#2EB5E0");		
		    				var id=$(this).attr('depId');
		    				var parentEndDate=$(this).attr('parentEndDate');
		    				$(this).parents(".fc-event-inner").css("background","#067886").css("border","#067886").parent('div').css("background","#067886").css("border","#067886");
		    				showDepTask(id,parentEndDate);
							event.stopPropagation();
   						});	
			        		parent.$('#loadingBar').hide();
			        		parent.timerControl("");
			        	},
				    });
            	}
				if(tName == getValues(Labels,'MY_TASKS')) {
				var myTasks = "";
					$.ajax({ 
				       url: Path+'/calenderAction.do',
				       type:"POST",
				       data:{act:'fetchMyTaskData'},
				       error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
				       success:function(result){ 
				       	parent.checkSessionTimeOut(result);
				       	myTasks = result.split("@|@")[0];
				        calendar.fullCalendar('addEventSource',eval(myTasks));
						$('#calendar').fullCalendar('render');
						var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        			if(!isiPad)
							$("#calendarContainer").mCustomScrollbar("update");
						listTaskIdNew=result.split("@|@")[1];
						var eachId=listTaskIdNew.split(",");
					//	$(".depTaskShowIcon").show();
	        	/*		for(var i=0;i < eachId.length;i++){
	        				$( "span[depId="+eachId[i]+"]" ).parent(".fc-event-inner").css("background","#993366").css("border","none");
	        				$( "span[depId="+eachId[i]+"]" ).parent(".fc-event-inner").parent(".fc-event").css("border","none").find(".fc-event-skin").css("border","none");
	        			}		*/
	        			$('.depTaskShowIcon').bind('click', function (event) {				
	        			//	$(".fc-event-inner").css("background","#2EB5E0").css("border","#2EB5E0").parent('div').css("background","#2EB5E0").css("border","#2EB5E0");		
		    				var id=$(this).attr('depId');
		    				var parentEndDate=$(this).attr('parentEndDate');
		    				$(this).parents(".fc-event-inner").css("background","#067886").css("border","#067886").parent('div').css("background","#067886").css("border","#067886");
		    				showDepTask(id,parentEndDate);
							/*	if(event.pageX < $("#calendarContainer").width()-40){
							    $('#depTaskPopDiv').css('left',event.pageX-75);    
							    $('#depTaskPopDiv').css('top',event.pageY-60);
							    $('#depTaskPopDiv').css('display','inline');     
							    $(".triangleDiv").css("margin-right",'374px');
						    }else{		
						    	var a = (event.pageX-$('#depTaskPopUpDiv').width());
						    //	var b = ($('#calendarContainer').width()-event.pageX);
							    $('#depTaskPopDiv').css('left',a);    
							    $('#depTaskPopDiv').css('top',event.pageY-60);
							    $('#depTaskPopDiv').css('display','inline');     
							    $(".triangleDiv").css("margin-left",event.pageY);
						    }*/
						    event.stopPropagation();
   						});	 
						parent.$('#loadingBar').hide();
						parent.timerControl("");
					  }
				    });
				 }
}


 function loadNotificationsFun(id,divId){
 	myTaskId=id;
 	parId = divId;	
	parent.menuFrame.location.href = Path+'/colAuth?pAct=calListView'
	$.ajax({ 
	           url: Path+'/calenderAction.do',
	       	   type:"POST",
		       data:{act:'getMyTaskDetails',taskId:id,parId:divId},
		       error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
		       success:function(result){
		         parent.checkSessionTimeOut(result);
	         	$("#eventPopup").hide();
	       		$("#taskPopupSection").hide();
       			$('.transparent').show();
       			parent.$("#transparentDiv").show();
       			$('#taskCalendar').append(result);
       			if(divId == 'myEvent' || divId == 'myEvents')	
       				$(".removeEventUser").hide();	
				if(height >= 550)
					$('.eventPopup').css('height','550px');
				else {
					$('.eventPopup').css({'height':height+'px','margin-top':'','margin-top':'-'+height/2+'px'});
				}
				if(divId == 'mySprintTask'){
					loadCustomLabel('TaskUI');
					$('.sprintUserContainer').css('width','715px');
					$('.sprintUserContainer').css('margin-left','20px');
					$('.userTitle').css('width','230px');
					$('.slider').css('width','235px');
					$('.completePer').css('width','50px');
					$('.userContainerSlide').css('width','150px');
					$('.taskAmount').css('margin-left','30px');
					$('.userStatus').css('width','155px');
					$('.userWorkingHour').css('text-align','left');
					$('.userComments').css('float','right');
					$('.usrComments').css('font-size','12px');
				}
				var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        	if(divId == 'myEvent' || divId == 'myEvents'){
	        		if(!isiPad){
       					scrollBar('postedComments');
       					scrollBar('eventUserContainer');
       					if(height < 550){
							$('.eventPopup').css('width','660px');
							scrollBar('myTaskPopup');
						}	
       				}	
       			}	
       			else {	
	       			taskHour = $('#workingHours').val();
	       			if(!isiPad){
						commentsScrollBar();
						if(height < 550){
							$('.eventPopup').css('width','820px');
							scrollBar('myTaskPopup');
						}	
					}	
				}
				$(".taskSub").each(function(){
                	var title = $(this).attr('title').replaceAll("CHR(41)"," ").replaceAll("CHR(39)","'");
                	$(this).attr('title',title);
				});
				parent.$('#loadingBar').hide();	
				parent.timerControl("");
			}
	  });
 }




 var newStartDate='';
 var newEndDate='';
  function setSnEdates(sprintId){
    var sDate=$('#startDateSprint').val().trim();
   var eDate=$('#endDateSprint').val().trim();
   newStartDate=$('div#ddList3').children('ul').find('li#'+sprintId).children('input.sprintStartDate').val();
   newEndDate=$('div#ddList3').children('ul').find('li#'+sprintId).children('input.sprintEndDate').val();
   if(sDate=='' || eDate==''){
      $('#startDateSprint').val(newStartDate);
      $('#endDateSprint').val(newEndDate);
      $('#taskSprint').val(sprintId);
   }else{
   		parent.confirmReset(getValues(customalertData,"Alert_Reset_Date"),"reset","confirmSprintName","cancelSprintEdit");
   }
 }
 function confirmSprintName(){
 	$('#startDateSprint').val(newStartDate);
    $('#endDateSprint').val(newEndDate);
    $('#taskSprint').val(sprintId);
 }
 function cancelSprintEdit(){
 	$('div#ddList3').children('ul').find('li').hide();
    var sprintGroup=$('#taskSprintGroup').val();
    var sprintNameId=$('#taskSprint').val();
    var sprintName=$('div#ddList3').children('ul').find('li#'+sprintNameId).children('a').text();
    $('div#ddList3').children('span').text(sprintName);
    $('div#ddList4').children('ul').find('li#'+sprintGroup).show();
    $('div#ddList3').children('ul').find('li.sprintLi_'+sprintGroup).show();
 }          


/*
 var wFTaskDateXml="<WFTaskDates>";
  var j = 0;
 function getWFDateXml(k){
     var TaskDateXml = "<taskHour id=\""+k+"\">";
     var i=1;
     estmHour=0;
     estmMinute=0;
	 $('.estimatedHourDiv').each(function() {
	 	 var status = $(this).children('input.edCheckbox').val();
		 var date = $(this).children('div').children('span.taskDate').text().trim();
		 var day = $(this).children('div').children('span.taskDay').text().trim();
		 var hour = $(this).children('div').children('input.eHour').val().trim();
		 var minute = $(this).children('div').children('input.eMin').val().trim();
		 if(!hour || hour == "undefined") {
		 	hour = 0;
		 }
		 if(!minute || minute== "undefined") {
		 	minute = 0;
		 }
		 estmHour = parseInt(estmHour) + parseInt(hour);
		 estmMinute = parseInt(estmMinute) + parseInt(minute);
		 TaskDateXml +="<taskDate id=\""+i+"\">"
		 TaskDateXml +="<date>"+date+"</date>";
		 TaskDateXml +="<day>"+day+"</day>";
		 TaskDateXml +="<hour>"+hour+"</hour>";
		 TaskDateXml +="<min>"+minute+"</min>";
		 TaskDateXml +="<status>"+status+"</status>";
		 TaskDateXml +="</taskDate>";
		 i++;
		
 	});
	TaskDateXml +="</taskHour>";
	$('#taskDateDetails_'+k).val(TaskDateXml);
	$('#estmatedMinute_'+k).val(estmMinute);
	$('#estmatedHour_'+k).val(estmHour);
	hideTaskHourDiv();
 }			
  function wFDateXml(k){
 	var TaskDateXml = "<taskHour id=\""+k+"\">";
	 var start = $('#wFStart_'+k).val();
 	 var end = $('#wFEnd_'+k).val();
 	 var strD=start.split("-");
	 var str_date=strD[2]+"/"+strD[0]+"/"+strD[1];
	 var strE=end.split("-");
	 var end_date=strE[2]+"/"+strE[0]+"/"+strE[1];
	var tskDates = new Array();
	var tskMonth = new Array();
	var tskYear = new Array();
	var tskDays = new Array();
	var weekday=new Array(7);
	weekday[0]=day0;
	weekday[1]=day1;
	weekday[2]=day2;
	weekday[3]=day3;
	weekday[4]=day4;
	weekday[5]=day5;
	weekday[6]=day6;
	tHour = $('#estmatedHour_'+k).val();
	var startDte = new Date(str_date);
	var futureDte = new Date(end_date);
	var tMinute = $('#estmatedMinute_'+k).val();
	var range = []
	var  mil = 86400000 //24h
	var curHour=0;
	var curMinute=0;
	var totalDay = (futureDte-startDte)/mil +1;
	var hr_day=tHour/totalDay;
	var hr_per_day=Math.round(hr_day);
	
	
	var min_day=tMinute/totalDay;
	var min_per_day=Math.round(min_day);
	
	var flag=0;
	var flag_min=0;
	var j=futureDte.getTime();
    if((tHour>0) && (hr_per_day==0) ){
	   hr_per_day=tHour;
	}
	if((tMinute>0) && (min_per_day==0) ){
	   min_per_day=tMinute;
	} 
	var j=1;
     for (var i=startDte.getTime(); i<=futureDte.getTime();i=i+mil) {
     	curHour = parseInt(curHour,10) + parseInt(hr_per_day,10);
     	curMinute = parseInt(curMinute,10) + parseInt(min_per_day,10);
    		if(curHour < tHour) {
    			hr_per_day = hr_per_day;
    			if(i==j){
		  	 hr_per_day= parseInt(tHour,10) - (parseInt(curHour,10) - parseInt(hr_per_day,10));
			} 
    		}
    		else {
    			if(flag == 0){
     				curHour = curHour - hr_per_day;
	       		hr_per_day = tHour - curHour;
	       		flag = 1;
      		}
      		else
      			hr_per_day = 0;
		}
		
		
		if(curMinute < tMinute) {
    		min_per_day = min_per_day;
    		if(i==j){
		  	 min_per_day= parseInt(tMinute,10) - (parseInt(curMinute,10) - parseInt(min_per_day,10));
			} 
    	}
    	else {
    			if(flag_min == 0){
     				curMinute = curMinute - min_per_day;
		       		min_per_day = tMinute - curMinute;
		       		flag_min = 1;
      		    }
      		    else
      			    min_per_day = 0;
		}
		tskDates[i] = new Date(i).getDate();
	    tskMonth[i] = new Date(i).getMonth()+1;
	    tskYear[i] = new Date(i).getFullYear();
	    tskDays[i] = new Date(i).getDay();
	    var day = weekday[tskDays[i]]; 
	    var month = tskMonth[i];
	    if(month < 10) {
	    	month = '0'+month;
	    }
	    if(tskDates[i] < 10) {
	    	tskDates[i] = '0'+tskDates[i];
	    }
	    
	  TaskDateXml +="<taskDate id=\""+j+"\">"
	  TaskDateXml +="<date>"+tskDates[i]+"/"+month+"/"+tskYear[i]+"</date>";
	  TaskDateXml +="<day>"+day+"</day>";
	  TaskDateXml +="<hour>"+hr_per_day+"</hour>";
	  TaskDateXml +="<min>"+min_per_day+"</min>";
	  TaskDateXml +="<status>Checked</status>";
	  TaskDateXml +="</taskDate>";
	  if((tHour>0) && (hr_per_day==0) ){
	  	flag = 1;
	  }
	  if((tMinute>0) && (min_per_day==0) ){
	  	flag_min = 1;
	  }
	 
	  j++;
	}	
	TaskDateXml +="</taskHour>";
	return TaskDateXml;
 }
*/

/*	function confirmDate(){
		 if(endId == 'endAfter') {
		 	if($('#noOfOccurences').val() > 0) {
		    	recEnd = $('#noOfOccurences').val()+' occurence(s)';
		    }		
		   	else {
		   		$('#noOfOccurences').css('border','1px solid red');
		   		return null;
		   	}	
	    }	
	    if(endId == 'endDate') {
	    	if($('#recEnd').val()) {
	   			var date = $('#recEnd').val().split('-');
	   			recEnd=date[2]+'-'+date[0]+'-'+date[1];
	   		}
		   	else {
		   		$('#recEnd').css('border','1px solid red');
		   		return null;
		   	}	
	    }
	    if(endId == 'noEnd') {
	    	recEnd= 'No End';
	    }
	   $('#recurrencePopupDiv').hide();
	}

*/ 		



 function uploadCsvFile(){
 	$("div#CreateNewCsvPopUp").css("display","block");parent.$("#transparentDiv").show();
 	$("#noteColumn").attr("href",lighttpdpath+"/csvFileTaskExample.csv");
 }
 $("#uploadCsvFileButton").click(function(){});
function submitCsvFileForm(e){
        $("#csvUploadFileName").text('');
		var FileName = $('#CsvFileName').val();
		var extension = FileName.split(".");
		if(extension[1] == "csv" || extension[1] == "CSV"){
			$("#csvUploadFileName").text(FileName);
			$("#uploadCsvFileButton").click(function(){
					var formname = e.form.name;
					var colform = document.getElementById(formname);
					colform.target = 'upload_target';
					$('#loadingBar').show();
					colform.submit();
				});
		}else{parent.alertFun(getValues(companyAlerts,"Alert_UpdCsv"),"warning");return false;
		}
	 }
	 
	function saveCsvFileCalendar(path,fileName,extn){
			$("#csvUploadFileName").text("");$("div#CreateNewCsvPopUp").css("display","none")
		$.ajax({
			 url: Path+'/calenderAction.do',
			type:"POST",
			data:{act:"saveCsvFileDetails",filepath:path,fileName : fileName},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		 success:function(result){
			    var data=result.split("#@#");
			    var emptyParticipant=data[2].substring(1,data[2].length-1)
			    if(data[0] == "success"){
			       parent.alertFun(getValues(companyAlerts,"Alert_NewTaskCrt"),'warning');$('#loadingBar').hide();
			    }
		  		    var result1=data[1].substring(1,data[1].length-1);
					if(result1.length!=0 || emptyParticipant.length!=0){
					   if(result1.length!=0 && emptyParticipant.length!=0){
					      parent.alertFun(getValues(companyAlerts,"Alert_TaskRow "+result1+" , "+emptyParticipant+" Alert_NotInsert"),'warning');
					    }
					   else if(result1.length==0 && emptyParticipant.length!=0){
					    parent.alertFun(getValues(companyAlerts,"Alert_TaskRow "+emptyParticipant+" Alert_NotInsert"),'warning');
					   }else {
					    parent.alertFun(getValues(companyAlerts,"Alert_TaskRow "+result1+" Alert_NotInsert"),'warning');
					   }
					   $('#loadingBar').hide();parent.$("#transparentDiv").hide();}
					   window.location.href = Path+"/colAuth?pAct=calMenu";  
			}  });   }
			
     //For upload document for task
     
     function uploadDocumentPopUp(){
        $("#uploadTaskLevelDoc").show();
        var docTaskType=$("#doctaskType").val();
        if(docTaskType == "assignedTasks" || docTaskType =="" || docTaskType == "assignedSprint" || docTaskType == "assignedIdea" || docTaskType == "event" || docTaskType == "assignedEvent"){
            $('#WsUploadOptns').slideToggle("slow");
        }
        if(docTaskType == "assignedTasks" || docTaskType == "assignedSprint" || docTaskType == "assignedIdea" || docTaskType == "event" || docTaskType == "assignedEvent"){
           $("#docTaskUpload").show();
           $("#docTaskContentSubDiv").css('height','273px');
        }else{
          if(docTaskType == "myTasks" || docTaskType == "mySprint" || docTaskType == "myIdea" || docTaskType == "myEvent" || docTaskType == "myEvents"){
             $("#docTaskUpload").hide();
             $("#docTaskContentSubDiv").css('height','310px');
           }
           
        }
        $("#overlay").show();
        $("#TaskLevellinkSection").hide();
        if(tflag == true){
           fetchDocdata('default');
        }
      }
     
     function fetchDocdata(type){
        parent.$("#loadingBar").show();
		parent.timerControl("start");
        var docTaskId=$("#uploadDocTaskId").val();	
	    var docTaskType=$("#doctaskType").val();
	    var docTaskprjId=$("#taskLevelprojectId").val();
	    if(docTaskType == "event"){
	      menuType="event";
	    }else{
	      menuType="cal";
	    }
        $.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
		   data:{act:'FetchTaskLevelDocuments',docTaskId:docTaskId,docTaskType:docTaskType,type:type,docTaskprjId:docTaskprjId,menuType:menuType},
		   error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		   success:function(result){
		       parent.checkSessionTimeOut(result);
		       tflag = false;
		       if(result == ""){
		          $("#docTaskContentSubDiv").html("<div align=\"center\" style=\"color:#000000;font-size:14px\">No documents found.</div>");
		       }else{
		          $("#docTaskContentSubDiv").html(result);
		       }
		       scrollBar('docTaskContentSubDiv');
		       if($("#docTaskContentSubDiv").find('div.mCSB_scrollTools').css('display') == 'block'){ 
		           $("#taskLevelFldHeader").css("width","98%");
		           $('#docTaskContentSubDiv .mCSB_container').css('margin-right','17px');
		           $(".hideOptionDiv").css('margin-right','25px');
		       }else{
		           $("#taskLevelFldHeader").css("width","99%");
		           $('#docTaskContentSubDiv .mCSB_container').css('margin-right','7px'); 
		           $(".hideOptionDiv").css('margin-right','40px');
		       }
		       if($('div#docTaskContentSubDiv').find('.mCSB_container').hasClass('mCS_no_scrollbar')){
			    		    $('div#docTaskContentSubDiv').children().children('.mCSB_container').css("position","static");
	    	     			//$('div#galleryAlbumContDiv').children().children('.mCSB_container').css("height","100%");
	    	      			$("div#docTaskContentSubDiv").mCustomScrollbar("update"); 
	    				}
		       parent.$("#loadingBar").hide();
			   parent.timerControl("");
		   }
		   });
     }
     function TaskDocUpload(){
        $('#WsUploadOptns').slideToggle("slow");
     }
     
     var glbDocId="";
     var glbTaskId="";
     var glbdelType="";
     function deleteTaskLevelDocument(docId,taskId,delType){
        glbDocId=docId;
        glbTaskId=taskId;
        glbdelType=delType;
        if(delType == 'link'){
          parent.confirmFun(getValues(companyAlerts,"Alert_DoDelLink"),"delete","deleteDocConfirm");
        }else{
           parent.confirmFun(getValues(companyAlerts,"Alert_DelFolDoc"),"delete","deleteDocConfirm");
        }
     }
     
     function deleteDocConfirm(){
        var docTaskType=$("#doctaskType").val();
         parent.$("#loadingBar").show();
		 parent.timerControl("start");
          if(glbdelType == 'link'){
		    $("#linkLevel_"+glbDocId).remove();
		 }else{
            $("#doc_"+glbDocId).remove();
         }
         $.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
		   data:{act:'DeleteTaskLevelDocuments',docId:glbDocId,docTaskType:docTaskType,taskId:glbTaskId,glbdelType:glbdelType},
		   error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		   success:function(result){
		       parent.checkSessionTimeOut(result);
		       if(result == "success"){
			       if(glbdelType == 'link'){
			        parent.alertFun(getValues(companyAlerts,"Alert_LinkDel"),'warning');
			       }else{
			        parent.alertFun(getValues(companyAlerts,"Alert_DocDel"),'warning');
			       }
		       }
		       $("div#docTaskContentSubDiv").mCustomScrollbar("update"); 
		        if($("#docTaskContentSubDiv").find('div.mCSB_scrollTools').css('display') == 'block'){ 
		           $("#taskLevelFldHeader").css("width","98%");
		           $('#docTaskContentSubDiv .mCSB_container').css('margin-right','17px');
		           $(".hideOptionDiv").css('margin-right','25px');
		       }else{
		           $("#taskLevelFldHeader").css("width","99%");
		           $('#docTaskContentSubDiv .mCSB_container').css('margin-right','7px'); 
		           $(".hideOptionDiv").css('margin-right','40px');
		       }
		       parent.$("#loadingBar").hide();
			   parent.timerControl("");
		   }
		   });
     }

